import os
import shutil
import mimetypes
import time

# Set up the directories to move files to
if os.name == "posix":  # Mac or Linux
    video_dir = os.path.expanduser("~/Videos")
    picture_dir = os.path.expanduser("~/Pictures")
elif os.name == "nt":  # Windows
    video_dir = os.path.join(os.path.expanduser("~"), "Videos")
    picture_dir = os.path.join(os.path.expanduser("~"), "Pictures") 
else:
    print("Unsupported operating system")
    exit()

while True:
    # Check each file on the desktop and move it to the appropriate directory
    desktop = os.path.expanduser("~/Desktop")
    for filename in os.listdir(desktop):
        filepath = os.path.join(desktop, filename)
        mimetype, _ = mimetypes.guess_type(filepath)
        if mimetype and mimetype.startswith("image/"):
            shutil.move(filepath, picture_dir)
        elif mimetype and mimetype.startswith("video/"):
            shutil.move(filepath, video_dir)

    # Wait for 1 seconds before checking again
    time.sleep(1)
