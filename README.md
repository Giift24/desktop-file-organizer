Desktop File Organizer
This project automatically organizes files on your desktop based on their file type. Files with an image mimetype are moved to the Pictures directory and files with a video mimetype are moved to the Videos directory.

How to Use
Clone the repository or download the code file.
Run the script using a Python interpreter.
Files on your desktop will be organized into the appropriate directory automatically.
Compatibility
The code is compatible with Mac, Linux, and Windows operating systems.